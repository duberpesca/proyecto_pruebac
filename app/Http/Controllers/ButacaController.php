<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Butaca;
class ButacaController extends Controller
{


    public function new_update_ajax(Request $request, $nombre){
            $arr1=$request->only('id_butaca','estado');
            $butaca = Butaca::find($arr1['id_butaca']);
            $butaca->estado=$arr1['estado'];
            $butaca->update();
            return json_encode(array('msg' =>$arr1['id_butaca']));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("butacas.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $butacas = Butaca::all();
        
        if(count($butacas)==0){
            for($fila=1;$fila<=5;$fila++){
                for ($columna=1; $columna <=10; $columna++) { 
                    $butaca = new Butaca();
                    $butaca->fila=$fila;
                    $butaca->columna=$columna;
                    $butaca->estado="Activa";
                    $butaca->save();
                }
            }
        }
        //activar desabilitar y no tiene eliminar y en reservas es en donde se inavilita por estar en uso
        return redirect("/butacas");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
