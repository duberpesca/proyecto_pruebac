<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
class ReservaController extends Controller
{


    public function new_update_ajax(Request $request, $nombre){
            $arr1=$request->only('id_butaca','fecha_reserva');
            $fech1= explode("/", $arr1['fecha_reserva']);
            $str1=$fech1[2]."/".$fech1[1]."/".$fech1[0];
            $listaUsuarios[]="<option value=''>--Seleccione--</option>";
            $reserva_usuario = DB::table('reserva_usuarios')
                    ->join('reservas','reserva_usuarios.id_reserva',"=",'reservas.id')
                    ->where('reserva_usuarios.id_butaca',"=",$arr1['id_butaca'])
                    ->where('reservas.fecha_asistencia',"=",$str1)->get() ;
            $butacaEstadoFecha="Reservada";
            if(count($reserva_usuario)==0){
                $butacaEstadoFecha="Disponible";
                $usuarios = User::where("estado","Activo")->get();
                
                foreach ($usuarios as $key => $usuario) {
                    $res_us = DB::table("reserva_usuarios")->join('reservas','reserva_usuarios.id_reserva',"=",'reservas.id')->where('reservas.fecha_asistencia',"=",$str1)->where("reserva_usuarios.id_usuario_final","=",$usuario->id)->get();
                    if(count($res_us)==0){
                        $listaUsuarios[]="<option value='".$usuario->id."'>".$usuario->name." ".$usuario->lastname."</option>";
                    }
                }

            }
           
            return  json_encode(array('msg' =>$butacaEstadoFecha,"lista_usuarios"=>$listaUsuarios));

    }
    public function reservar_ajax(Request $request,$nombre){

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reservas.index');
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
