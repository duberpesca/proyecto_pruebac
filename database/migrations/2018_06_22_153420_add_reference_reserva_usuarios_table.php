<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferenceReservaUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reserva_usuarios', function (Blueprint $table) {
            $table->foreign('id_butaca')->references('id')->on('butacas');//tuve que agregar esta migracion porque al intentar generar en reservas_usuarios esta referencia  a butaca daba error como la tabla butaca no existe en el momento de creacion al ejecutar el comando php artisan migrate es necesario ordenarle que al finalizar cree la refencia ya abiendo creado la tabla butacas
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reserva_usuarios', function (Blueprint $table) {
            //
        });
    }
}
