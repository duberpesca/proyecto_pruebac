<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservaUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserva_usuarios', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('id_reserva')->unsigned();
            $table->foreign('id_reserva')->references('id')->on('reservas');
            $table->integer('id_butaca')->unsigned();
            
            $table->integer('id_usuario_final')->unsigned();//Cliente ocupara butaca
            $table->foreign('id_usuario_final')->references('id')->on('users');
            $table->enum('estado', array('Activa', 'Cancelada'));

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserva_usuarios');
    }
}
