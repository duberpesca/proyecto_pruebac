<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->date('fecha_asistencia');//aqui guardo fecha asistencia y fecha esta en laravel column 
            $table->integer('id_usuario_titular')->unsigned();  //creo campo      
            $table->foreign('id_usuario_titular')->references('id')->on('users'); //asigno referencia
            $table->enum('estado', array('Activa', 'Cancelada'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
