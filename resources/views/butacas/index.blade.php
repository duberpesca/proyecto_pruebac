@extends('layouts.app')
@section('title','Butacas')
@section('content')
<?php use App\Butaca;
    $butacas = Butaca::orderBy('fila')->orderBy('columna')->get();
 ?>
	 <!-- este div con class container me sirvio para centrar todo wee-->

			
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Butacas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form method="POST" action="/butacas">  
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" <?= (count($butacas)!=0)? "disabled":"" ?> class="btn btn-success">Cargar Butacas<img src="/images/add-icon.png" height="10px;" width="10px"></button>
                            </form>

                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-15">
                                    <table>
                                        <?php $x1=0 ?> 
                                        @foreach($butacas as $butaca)
                                        <?php $x1=$x1+1; ?>
                                        <?php if($x1==1){?>
                                        <tr>
                                        <?php } 
                                        if($butaca->estado=="Activa"){
                                                    $estado1="selected";
                                                    $estado2="";
                                           }else{
                                                    $estado2="selected";
                                                    $estado1="";
                                           }
                                        ?>                                            
                                        <td>
                                            <div class='col-sm-13'>
                                            <div class="form-group" >
                                                    <img src="/images/img-butaca2.jpg"  width="50px" height="50px"><span>{{$butaca->fila}}.{{$butaca->columna}}</span><br>
                                                    
                                        <select onchange="estado_butaca(this);" class="form-control" data-id="{{$butaca->id}}" value="{{$butaca->estado}}">
                                                        <option value="Activa" {{$estado1}}>Activar</option>
                                                        <option value="Desabilitada" {{$estado2}}>Desabilitar</option>
                                                    </select><br>
               
            
                                            </div>
                                        </div>
                                        </td>
                                        <?php if($x1==10){ ?>
                                        </tr>
                                        <?php $x1=0;} ?>
                                                
                                        @endforeach                                        
                                    </table>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        
		<script type="text/javascript">
           function estado_butaca (elm){
            
            var x =$(elm).attr("data-id");
            $.get("/butacas/new_update_ajax",{estado:$(elm).val(),id_butaca:x},function(msg){
                    //mensaje pero no lo coloco de satisfactorio actualiza butaca
            },'json');
                
           }   
                 
        </script>
<style type="text/css">
    td{
        width: 150px;
        height: 100px;
    }
</style>
@endsection