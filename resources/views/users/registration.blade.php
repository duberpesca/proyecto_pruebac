@extends('layouts.registration')
@section('title','Registrate')
@section('content')
 <div class="row">
                <div class="col-lg-8" style="margin: 0 auto;">
                    <div class="panel panel-default blog_nota">
                        <div class="panel-heading blog_nota" style="background-color: #ffbf00">
                            <h1>Formulario de Registro</h1>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12" style="background-color: red;margin: 0 auto;">

<form class="form-signin blog_nota" role="form" style="margin-top: 2%; margin-bottom:  2%;" method="POST" action="/users" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_vr1" value="reg0001">
      
      <table style="margin: 0 auto;" id="tb1">
        <tbody>
          <tr>
            <td class="ancho_td">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="name" placeholder="Nombres">
                </div> 
            </td>
            <td class="td_espacio"></td>
            <td class="ancho_td">
               <div class="form-group">
                  <label>Apellidos</label>
                  <input type="text" class="form-control" name="lastname" placeholder="Apellidos">
               </div>  
            </td>
          </tr>
          <tr>
            <td class="ancho_td">
               <div class="form-group">
                    <label>DNI</label>
                    <input type="text" class="form-control" name="dni" placeholder="DNI">
                </div>        
            </td>
           <td class="td_espacio"></td>
            <td class="ancho_td">
              <div class="form-group">
                 <label>Email</label>
                   <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required>
               </div>   
            </td>
          </tr>
          <tr>
            <td class="ancho_td">
              <div class="form-group">
          <label>Password</label>
          <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        </div>
            </td>
            <td class="td_espacio"></td>
            <td class="ancho_td">
              <div class="form-group">
          <label>Avatar</label>
          <input type="file" name="avatar" id="avatar" class="form-control" accept="image/*">
        </div>
            </td>
          </tr>
          
        </tbody>
      </table>
      <div class="form-group">
          <img id="img_icon" class="card-img-top rounded-circle mx-auto d-block" src="/images/default-user.png" alt="Icon" style="height: 100px;width: 108px;background-color: #EFEFEF;margin: 20px;">
        </div>
      
      

         
      
       
      
      

<div class="form-group">
      <button class="form-control btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
      </div>
      <p  class="color_text">¿Ya eres miembro de nuestro sistema?<br> por favor inicia sesión dando click en <br><a href="{{url('/')}}" style="color: #ffbf00;"><b style="font-size: 150%;">Iniciar Sesión</b></a></p> 
      
      <p class="mt-5 mb-3 text-muted">  &copy; 2018</p>
    </form>
  

                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
          </div>              
	  
    <style type="text/css">

     
      .blog_nota { 
        background-color: #FFFFFF; border: 1px solid #DBDF00;  overflow: auto;-moz-box-shadow: 8px 9px 5px #000000; -webkit-box-shadow: 8px 9px 5px #000000; box-shadow: 8px 9px 5px #000000; 
      }
      .ancho_td{
        width: 50%;
      }
      .td_espacio{
        width:1%;
      }
    </style>
    <script type="text/javascript">
  $("#avatar").change(function(evt){


      readURL(this);
  });
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#img_icon').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endsection