@extends('layouts.login')
@section('title','Login')
@section('content')

    <form class="form-signin blog_nota" style="background-color: red;" method="POST" action="/users">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_vr1" value="log_0001">
      <h1 class="h3 mb-3 font-weight-normal color_text">Inicio de Sesión</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
      <label for="inputPassword"  class="sr-only">Password</label>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
      <div class="checkbox mb-3" >
        <label>
          <input type="checkbox" name="is_admin" value="false"> Es Administrador
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesión</button>
      <p  class="color_text" >¿No eres usuario de nuestro sistema?<br> por favor registrate dando click en <br><a href="{{ url('/users/create') }}" style="color: #FFFF00;"><b style="font-size: 150%;">Registrar</b></a></p> 
      <p class="mt-5 mb-3 text-muted">  &copy; 2018</p>
    </form>
    
    <style type="text/css">

      .color_text{
        color: #FFFFFF
      }
      .blog_nota { 
        background-color: #FFFFFF; border: 1px solid #DBDF00;  overflow: auto;-moz-box-shadow: 8px 9px 5px #000000; -webkit-box-shadow: 8px 9px 5px #000000; box-shadow: 8px 9px 5px #000000; 
      }

    </style>
    
@endsection